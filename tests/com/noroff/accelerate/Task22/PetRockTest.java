package com.noroff.accelerate.Task22;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PetRockTest {
    private PetRock rocky;

    @BeforeEach
    void myTestSetup() {
        rocky = new PetRock("Rocky");
    }

    @Test
    void getName() {
        assertEquals("Rocky", rocky.getName());
    }
    @Test
    void testUnhappyToStart () {
        assertFalse(rocky.isHappy());
    }
    @Test
    void testHappyAfterPlay() {
        rocky.playWithRock();
        assertTrue(rocky.isHappy());
    }

    @Disabled("Exception throwing not yet defined")
    @Test
    void nameFail() {
        assertThrows(IllegalStateException.class, () -> {rocky.printHappyMessage();});
    }

    @Test
    void testFavNum() {
        assertEquals(42, rocky.getFavNumber());
    }

    @Test
    void emptyNameFail() {
        assertThrows(IllegalArgumentException.class,() -> {new PetRock("");});
    }
}