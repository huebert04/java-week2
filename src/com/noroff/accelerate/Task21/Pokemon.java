package com.noroff.accelerate.Task21;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

public class Pokemon {
    public Pokemon() {
        getPokemonInfo();
    }

    private void getPokemonInfo() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter Pokemon you want to search:");
            String pokemon = sc.nextLine();

            URL url = new URL("https://pokeapi.co/api/v2/pokemon/" + pokemon.toLowerCase());
            String readLine = null;
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            int responseCode = con.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                StringBuilder response = new StringBuilder();
                while ((readLine = in.readLine()) != null) {
                    response.append(readLine);
                }
                in.close();

                JSONObject obj = new JSONObject(response.toString());
                String name = obj.getString("name");
                int weight = obj.getInt("weight");
                int height = obj.getInt("height");
                int be = obj.getInt("base_experience");
                System.out.println("Pokemon Information: \n" + "Name: " + name + "\nHeight: " + height + "\nWeight: " + weight + "\nBase experience: " + be + "\n");
                JSONArray arr = obj.getJSONArray("types");
                JSONObject type = null;
                String typeName = "";

                if (arr.length() == 1) {
                    type = arr.getJSONObject(0).getJSONObject("type");
                    typeName = type.getString("name");
                    System.out.println("Pokemons with the same " + typeName + " type:");
                    getPokemonsWithType(typeName);
                } else {
                    ArrayList<String> result = new ArrayList<String>();
                    for (int i = 0; i < arr.length(); i++) {
                        type = arr.getJSONObject(i).getJSONObject("type");
                        typeName = type.getString("name");
                        result.add(typeName);
                    }
                    System.out.println("Which type would you like to see a list of pokemon?");
                    System.out.println("1: " + result.get(0));
                    System.out.println("2: " + result.get(1));
                    int choice = Integer.parseInt(sc.nextLine());
                    switch (choice) {
                        case 1:
                            System.out.println("Pokemons with the same " + result.get(0) + " type:");
                            getPokemonsWithType(result.get(0));
                            break;
                        case 2:
                            System.out.println("Pokemons with the same " + result.get(1) + " type:");
                            getPokemonsWithType(result.get(1));
                            break;
                        default:
                            System.out.println("Wrong input.");
                    }
                }
                con.disconnect();

            } else {
                System.out.println("Pokemon was not found.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void getPokemonsWithType(String typeName) {
        try {
            URL url = new URL("https://pokeapi.co/api/v2/type/" + typeName.toLowerCase());
            String readLine = null;
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            int responseCode = con.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                StringBuilder response = new StringBuilder();
                while ((readLine = in.readLine()) != null) {
                    response.append(readLine);
                }
                in.close();

                JSONObject obj = new JSONObject(response.toString());
                JSONArray arr = obj.getJSONArray("pokemon");
                for (int i = 0; i < arr.length(); i++) {
                    JSONObject pokemonList = arr.getJSONObject(i).getJSONObject("pokemon");
                    String pokemonName = pokemonList.getString("name");
                    System.out.println(pokemonName);
                }
                con.disconnect();

            } else {
                System.out.println("DID NOT FIND POKEMONS.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
