package com.noroff.accelerate.Task20;

import com.noroff.accelerate.Task20.Moves.Movable;
import com.noroff.accelerate.Task20.AnimalType.Carnivore;

import java.util.*;

public class Bird extends Carnivore {

    public Bird(String name, ArrayList<Movable> moves) {
        super(name, moves);
    }

    public String details() {
        return getName() + super.detail() + "\n" + 
               getName() + " can " + super.moves() + "\n" +
               getName() + super.shuffle();
    }
}