package com.noroff.accelerate.Task20;

import java.util.ArrayList;
import com.noroff.accelerate.Task20.Moves.*;

public class Program {
    public Program() {
        getAnimals();
    }
    private void getAnimals() {
        ArrayList<Movable> dogmoves = new ArrayList<Movable>();
        ArrayList<Movable> birdmoves = new ArrayList<Movable>();

        dogmoves.add(new Walk());
        dogmoves.add(new Run());
        dogmoves.add(new Climb());

        birdmoves.add(new Walk());
        birdmoves.add(new Fly());

        Dog dog1 = new Dog("Doggo", dogmoves);
        Bird bird1 = new Bird("Birdy", birdmoves);

        System.out.println(dog1.details());
        System.out.println();
        System.out.println(bird1.details());

    }
}