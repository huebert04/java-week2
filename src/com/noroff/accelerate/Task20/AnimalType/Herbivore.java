package com.noroff.accelerate.Task20.AnimalType;

import com.noroff.accelerate.Task20.Moves.Movable;
import java.util.ArrayList;


public abstract class Herbivore extends Animal {
    public Herbivore(String name, ArrayList<Movable> moves) {
        super(name, moves);
    }

    public String detail() {
        return " is a Herbivore";
    }
}