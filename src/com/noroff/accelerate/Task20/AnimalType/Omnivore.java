package com.noroff.accelerate.Task20.AnimalType;

import java.util.ArrayList;
import com.noroff.accelerate.Task20.Moves.Movable;

public abstract class Omnivore extends Animal {
    public Omnivore(String name, ArrayList<Movable> moves) {
        super(name, moves);
    }

    public String detail() {
        return " is a Herbivore";
    }
}