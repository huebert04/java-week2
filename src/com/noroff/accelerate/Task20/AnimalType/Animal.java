package com.noroff.accelerate.Task20.AnimalType;

import com.noroff.accelerate.Task20.Moves.Movable;

import java.util.*;

public abstract class Animal {
    public String name;
    protected ArrayList<Movable> moves;

    public Animal(String name, ArrayList<Movable> moves){
        this.name = name;
        this.moves = moves;
    } 

    public String getName() {
        return name;
    }

    public String moves() {
        
        StringJoiner result = new StringJoiner(", ");

        for(Movable move: moves) {
            result.add(move.getName());
        }

        return result.toString();
    }

    public String shuffle() {
        Collections.shuffle(moves);
        return moves.get(0).move();
    }
}