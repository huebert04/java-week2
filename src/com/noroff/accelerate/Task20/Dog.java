package com.noroff.accelerate.Task20;

import com.noroff.accelerate.Task20.Moves.Movable;
import com.noroff.accelerate.Task20.AnimalType.Omnivore;

import java.util.*;

public class Dog extends Omnivore {

    public Dog(String name, ArrayList<Movable> moves) {
        super(name, moves);
    }

    public String details() {
        return super.getName() + super.detail() + "\n" + 
               super.getName() + " can " + super.moves() + "\n" +
               super.getName() + super.shuffle();
    }
}