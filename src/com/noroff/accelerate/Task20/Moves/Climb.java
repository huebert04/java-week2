package com.noroff.accelerate.Task20.Moves;

public class Climb implements Movable {
    public String getName() {
        return "Climb";
    }

    public String move() {
        return " is climbing.";
    }
}