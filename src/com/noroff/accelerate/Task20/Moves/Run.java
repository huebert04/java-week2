package com.noroff.accelerate.Task20.Moves;

public class Run implements Movable {
    public String getName() {
        return "Run";
    } 
    public String move() { 
        return " is running.";
    }
}