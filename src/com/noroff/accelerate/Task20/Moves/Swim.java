package com.noroff.accelerate.Task20.Moves;

public class Swim implements Movable {
    public String getName() {
        return "Swim";
    }

    public String move() {
        return " is swimming.";
    }
}