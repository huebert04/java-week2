package com.noroff.accelerate.Task20.Moves;

public class Walk implements Movable {
    public String getName() {
        return "Walk";
    }
    
    public String move() {
        return " is walking.";
    }
}