package com.noroff.accelerate.Task20.Moves;

public class Fly implements Movable {
    public String getName() {
        return "Fly";
    }

    public String move() {
        return " is flying.";
    }
}