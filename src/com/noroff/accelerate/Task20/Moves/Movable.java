package com.noroff.accelerate.Task20.Moves;

public interface Movable {
    public abstract String getName();
    public abstract String move();
}