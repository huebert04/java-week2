package com.noroff.accelerate.Task22;

public class PetRock {
    private String name;
    private boolean happy = false;

    public PetRock(String name) {
        if(name.isEmpty()) {
            throw new IllegalArgumentException();
        }
        this.name = name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isHappy() {
        return happy;
    }

    public void playWithRock() {
        happy = true;
    }

    public String printHappyMessage() {
        //if(!happy) {
        //    throw new IllegalStateException();
        //}
        return "I'm happy";
    }

    public int getFavNumber() {
        return 42;
    }
}
