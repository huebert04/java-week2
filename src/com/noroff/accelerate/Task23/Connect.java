package com.noroff.accelerate.Task23;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect {
    public static Connection connect() {
        Connection conn = null;
        try {
            String url = "jdbc:sqlite::resource:Northwind_small.sqlite";
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.print(e.getMessage());
        }
        return conn;
    }
}
