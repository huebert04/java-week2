package com.noroff.accelerate.Task23;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Query {
    Connection conn = Connect.connect();

    public Query() {
        getQuery();
    }

    private void getQuery() {
        try {
            String query = "SELECT ProductName, UnitPrice FROM Product ORDER BY UnitPrice DESC ";
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(query);
            while (rs.next()) {
                System.out.println(rs.getString("ProductName") + "\t"
                        + rs.getBigDecimal("UnitPrice") + "\t");
            }
        } catch (
                SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }

        }
    }
}
